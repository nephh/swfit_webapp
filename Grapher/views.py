from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
import json
from .Utils import *
from .codeParser import *
import jsonpickle

class SrcGrapherView(APIView):

    def get(self, format=None):
        return Response(None)

    def post(self, request):
        # ssh = CodeParser().start((request.data).get("code"))
        # dd = json.dumps(ssh, default=lambda x: x.__dict__)
        # print(dd)
        # return Response(jsonpickle.encode(ssh), status=status.HTTP_202_ACCEPTED)
        # return Response(dd, status=status.HTTP_202_ACCEPTED)
        return Response(JsonEncoder().encode(CodeParser().start((request.data).get("code"))),
                        status=status.HTTP_202_ACCEPTED)
