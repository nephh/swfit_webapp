FROM python:3
MAINTAINER he.amirhossein@gmail.com

RUN mkdir /code
WORKDIR /code
COPY . /code/
RUN pip install -r /code/requirements.txt
RUN chmod +x /code/start.sh
#RUN apt update && apt-get install -y clang nodejs npm
RUN apt update && apt-get install -y clang
WORKDIR /usr/lib/x86_64-linux-gnu/
RUN ln -s libclang-7.so.1 libclang.so
#RUN npm install -g @angular/cli
WORKDIR /code
EXPOSE 8000
EXPOSE 4200
#CMD ["/code/start.sh"]